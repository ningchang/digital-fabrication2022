+++
image = "portfolio/img/week9/wk933.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 09: Computer controlled machine"
draft = false
weight = 2
+++

---
<!--more-->
> Computer controlled machine 

In this session, I spent much time modifying the files and how to load big plywood on a CNC machine.

##### Software
Fusion360
Adobe Illustrator
VCarve Pro

##### Notes
###### Files for CNC cutting
In Fusion 360: 
1. Use parameters for dimensions when making a joint. 
2. Create clean files: remove unnecessary lines. 
3. Make sure each connection is closed: If there are openings, it cannot create fillets on corners.
![image](../img/week9/wk922.png)
![image](../img/week9/wk921.png)


> ##### Vcarve Pro:
###### File:
1. Leave more spaces around the edges of the layout if the material is too big and needs clamps to fix on the vacuum table.

###### Material session:
1. The Zero Z on the” material surface” and “Machine bed” will influence the operations orders.
2. Material surface: upload the material first and set zero Z.
Machine bed: set the zero Z on the buffer layers and then load the material.
3. The starting point: is generally set as the southern-west corner. (It is essential!)

###### Outline session:
1. Tools setting and calculations
2. Add tabs : 

NOT ONLY add tabs on the big pieces, ALSO add tabs on the smaller holes to prevent from the small pieces material influence the whole cutting.

Preview session: Remember to “Reset the preview”, or the old preview will influence the new preview.


> ##### CNC machine:
1. Do a test before cutting a big piece:
2. Use a small piece of material to test. 
3. Test for the snug joint: there is a 2 mm difference between my expected dimension and the cutting piece. So, I need to adjust the dimension on my files.

###### Load the material
1. Make sure the holes on the vacuum table are OPEN (remove the lids and put them in the toolbox for storage ).
2. Check the Zero Z of the file is set as “Material surface ” or “Machine bed”.
3. Load the sacrifice layer (the buffer layer) first, then the material.
4. Use clamps if the material is too big. The locations of clamps should not hinder the route of the drilling tool.
5. After using clamps, if the material can Not attach to the table properly, it is better to cut it into smaller pieces (Then remember to measure the dimension and modify the file). 


###### Load the tool
1. Put the sponge under the tool holder before loading the tool.
2. Screw the tool tight. (Important !).
3. Make sure the tool matches the setting on the file. 
4. Turn on the vacuum, then Set the zero Z axis. (Depending on if you have already loaded the materials and the file setting).
Lift the tool higher while moving the tool, preventing the drilling tool from being hindered by clamps, material or something else.
5. Remember to put on dust shoes. 

###### Mach 3 (to operate CNC machine)
1. Reset!
2. Make sure ”Soft limits” are surrounded by the green line.
3. If there is a warning mentioned about “soft limits”, press “Regenerate tool route”.
![image](../img/week9/wk935.png)
![image](../img/week9/wk936.png)

> ##### CNC machine
1. Turn on the machine
2. Open the vacuum 
3. CLOSE the door. (If the door is open, the machine cannot work)
4. Press the Green button to start the spindle.
5. Press the Red button to STOP.


> ##### VCarve Pro 

###### Job setup
- choose Single Sided
- Set Job Size : this time the thickness of plywood are not leveling, so I set the thickness 15 (this photo set as 15.2 mm)

- Z Zero position : this step is important! Generally, choose "Machine bed"

- Datum Position : Chooese south western one

- Hit OK

###### Import Vectors
![image](../img/week9/wk902.png)

Make a test first.
![image](../img/week9/wk903.png)
![image](../img/week9/wk904.png)

###### Create Fillet
![image](../img/week9/wk905.png)

choose Dog-Bone
If could not create a fillet, there could be some openings, which need to be closed.

The tool Radius should be set as half of the radius.
If I choose 3mm, the setting should be 1.5mm plus 0.1, so the setting should be 1.6 mm here. 
![image](../img/week9/wk906.png)
![image](../img/week9/wk907.png)

###### Create toothpath
1. Select the vectors and click this icon
![image](../img/week9/wk908.png)

2. Select Tool
![image](../img/week9/wk909.png)

> ###### Caculate the Tool setting

Pass Depth: depends on the tool you’re gonna to use

Stepover: it is important when you are clearing a pocket how many mm or percentage of the tool diameter you want to hop over in order to clear some certain area

Feeds and Speeds: the most important part in CNC milling

Spindle speed: The machine can reach maximum 18000 rpm,
But don’t want to max the machine too much, so here is why choosing “16000 rpm”

XY speed of xy movement should in the range of speed
Plunge Rate: the vertical movement, 1/4 or 1/5 of feed speed

###### How to calculate feed rate?
The tool for soft wood, we can choose “chip load” of 0.035
And need to know number of flutes: here are 2 flutes

Calculation of Feed rate:
Feed rate = Chip load* Number of flutes* Spindle speed 

Chip load is 0.035
Number of flutes is 2

0.035*2 =0.07

Spindle speed is 16000 rpm

0.07*16000= 1120

Plunge rate = Feed rate /4 = 280  mm/min

1. take the reference first.

![image](../img/week9/wk910.png)
![image](../img/week9/wk911.png)
![image](../img/week9/wk912.png)
![image](../img/week9/wk913.png)
![image](../img/week9/wk914.png)
![image](../img/week9/wk915.png)
![image](../img/week9/wk916.png)
![image](../img/week9/wk917.png)
![image](../img/week9/wk918.png)
![image](../img/week9/wk919.png)
![image](../img/week9/wk920.png)
![image](../img/week9/wk921.png)
![image](../img/week9/wk922.png)
![image](../img/week9/wk923.png)
![image](../img/week9/wk924.png)
![image](../img/week9/wk925.png)
![image](../img/week9/wk926.png)
![image](../img/week9/wk927.png)
![image](../img/week9/wk928.png)
![image](../img/week9/wk929.png)
![image](../img/week9/wk930.png)
![image](../img/week9/wk931.png)
![image](../img/week9/wk932.png)
![image](../img/week9/wk933.png)
![image](../img/week9/wk934.png)
![image](../img/week9/wk935.png)
![image](../img/week9/wk936.png)
![image](../img/week9/wk937.png)
![image](../img/week9/wk938.png)
![image](../img/week9/wk939.png)
![image](../img/week9/wk940.png)
![image](../img/week9/wk941.png)