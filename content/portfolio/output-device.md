+++
image = "portfolio/img/week11/op_11.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 11: Output Devices"
draft = false
weight = 2
+++

---
<!--more-->
> Output Devices

In this session, I would like to take NeoPixels as the output device, aiming to design a breathing lamp (I haven’t reached this goal now…). 


##### 1. Test with a breadboard
Initially, I drafted a circuit with KiCad and took NeoPixel WS2812. Then, I would like to test the programming with a breadboard.
However, when I saw the physical WS2812, it was tiny and had no pin to insert the breadboard, so I needed to do the soldering first. 
And I took the suggestions from Yu-Han that I can take the “NeoPixel Diffused 5mm Through-Hole LED “ to test. 

##### 2. Concept(Ideal performance) --> Select a suitable output device
My ideal goal is to reach breathing-like lighting, which can gradually increase and decrease the brightness. However, this NeoPixel can perform different colours, which probably requires complicated coding. After discussing with Yu-Han and watching similar cases, it can control the amounts of LEDs to reach a breathing-like performance. 
So I tried simple programming first. See the video below.

##### current status
Tried to do the breathing-like programming, but didn't work.

![image](../img/week11/op_07.png)

![image](../img/week11/op_08.png)

![image](../img/week11/op_09.png)



#### Process
![image](../img/week11/op_01.png)

##### WS2812
![image](../img/week11/op_02.png)

##### NeoPixel Diffused 5mm Through-Hole LED
This NeoPixel contains datain,+5v, ground, and data out.

![image](../img/week11/op_03.png)

![image](../img/week11/op_04.png)

##### Test connection and programming with a bread board

![image](../img/week11/op_05.png)

![image](../img/week11/op_06.png)

{{<video src="../img/week11/op_01.mp4" controls="yes" width="640" height="385">}}

![image](../img/week11/op_11.png)

![image](../img/week11/op_10.png)



