+++
image = "portfolio/img/week13/ip_03.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 13: Input Devices"
draft = false
weight = 2
+++

---
<!--more-->
> Input Devices
Infrared barrier module - Obstacle sensor

> ##### Current status

The signal LED can detect the object. However, the NeoPixels cannot respond, and I'm still working on it.
![image](../img/week13/ip_05.png)

![image](../img/week13/ip_01.png)

![image](../img/week13/ip_02.png)

![image](../img/week13/ip_03.png)

{{<video src="../img/week13/ip_04.mp4" controls="yes" width="640" height="385">}}
