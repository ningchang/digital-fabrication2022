+++
image = "portfolio/img/week8/ele_d44.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 08: Electronics design"
draft = false
weight = 2
+++

This week is to design electronic circuits with KiCad. When drawing the circuit, there are a few principles which need to follow:
<!--more-->

##### Software

###### KiCad
https://www.kicad.org/download/macos/

##### Schematic editor
1. Must contain Power (PWR) and Ground (GND)
2. Each output device (e.g., LED) connects to a resistor to prevent shortcuts.
3. One end of each input (button, switch) and output (LED) device connects GND.
4. Calculate the voltages, currents and resistances, then select a suitable resistor or device.
5. Check datasheet.
6. Using labels can make the layout more flexible.
7. Mark a cross on unused “pins”.

##### PCB editor
1. The width of traces needs to be aware. If the trace is too thin, the resistance of the trace increases. 
2. Leave more space (space, gap) between traces. If the distance between traces is too narrow, the circuit may not work.
3. Considering manufacturing, the angles of traces should not be too sharp (e.g., right angle 90 degrees).

> #### WORKING ENVIRONMENT

Initially, to download KiCad and the symbol library and footprints for drawing circuits.

There are two ways to install symbol libraries in KiCad. One is to clone the data; another is to download them. However, as in the picture, I could not clone the symbol library on my laptop.

![image](../img/week8/ele_d01_download_kicad.png)  

###### Symbol library
So, I downloaded the library, and then I can add a symbol to my design.

![image](../img/week8/ele_d02_kicad_library.png)

![image](../img/week8/ele_d03_install_sym.png) 

##### Footprint
Click the “fab.pretty” to obtain the footprint ( elements?).

![image](../img/week8/ele_d05_kicad_footprint.png) 

> #### Design Circuits
> ##### Create a new folder and project

Name the folder”XIAOShieldBasic”, and click. The schematic one.

![image](../img/week8/ele_d04_newfolder.png)

![image](../img/week8/ele_d06_new_project.png) 

> ##### Start with Schematic editor

In this process, the first step is to place symbols and connect them with wires.
And an electronic circuit contains boards, power (PWR), and ground (GND).
Output devices and input devices connect GND.  

![image](../img/week8/ele_d07_schemetic_editor.png) 

##### 1. Add Board

Place the selected board.

![image](../img/week8/ele_d08_sym_place_xiaoboard_0.png) 

![image](../img/week8/ele_d09_sym_place_xiaoboard_1.png)

##### 2. Add Power (PWR)

Select 3V3 as the power.

![image](../img/week8/ele_d10_sym_place_power_0.png) 

![image](../img/week8/ele_d11_sym_place_power_1.png) 

![image](../img/week8/ele_d12_sym_place_power_2.png) 

##### 3. Add Ground (GND)

![image](../img/week8/ele_d13_sym_place_ground_0.png) 

![image](../img/week8/ele_d14_sym_place_ground_11.png) 

##### 4. Connect with wires

![image](../img/week8/ele_d15.png) 

![image](../img/week8/ele_d16.png) 

##### 5. Add LED (output device)

Here take LED_1206. To obtain more information, go and check the datasheet.

![image](../img/week8/ele_d17.png) 

##### 6. Check datasheet

The datasheet shows the forward voltage of LED_1206 is 2V.

The information is essential when calculating the currents and which resistor is suitable for use afterwards.
###### Go to Academy website and check the data sheet http://inventory.fabcloud.io/

![image](../img/week8/ele_d18.png) 


##### 7. Add a resistor (Prevent shortcuts)
Select R_1206
![image](../img/week8/ele_d19.png) 

![image](../img/week8/ele_d20.png) 
![image](../img/week8/ele_d21.png) 

![image](../img/week8/ele_d22.png) 
![image](../img/week8/ele_d23.png) 
![image](../img/week8/ele_d24.png)

##### 8. Connect LED, resistor, and GND

While placing the symbols, hit”R” can rotate the dimensions
![image](../img/week8/ele_d25.png) 

##### 9. Add texts

Make notes when we calculate the voltages, currents, and resistance.

![image](../img/week8/ele_d26.png) 

##### 10. Calculation

Since the power (total) of the board is 3V3, and the current is 30 mA (0.03A),
We need to calculate which resistor is suitable for use.

Here, the forward voltage of the LED is 2V, and there are two LEDs in the circuit. 
The calculations are in the pictures.

![image](../img/week8/ele_d27.png) 

![image](../img/week8/ele_d28.png) 

##### 11. Select suitable resistor

The resistance I got is 86.7 OHM. So I selected a resistor with 100 OHM, which resistance is close to what I calculated. 

![image](../img/week8/ele_d31.png) 

![image](../img/week8/ele_d29.png) 

##### 12. Add buttons (Switches)

 To make the layout clear and flexible, I placed symbols of buttons aside and marked the wires with labels. 

![image](../img/week8/ele_d32.png) 

##### 13. Mark a cross on unused pins

![image](../img/week8/ele_d33.png) 

##### 14. Check the electrical rules

Press"RUN ERC" 

![image](../img/week8/ele_d35.png) 

If everything goes well, it will show this.
![image](../img/week8/ele_d36.png) 

Then, continue with PCB editor.

> #### Then, PCB editor

The window looks like this

![image](../img/week8/ele_d37.png) 

##### 1. Update PCB from schematic

Press "UPDATE PCB".

![image](../img/week8/ele_d38.png) 

![image](../img/week8/ele_d39.png) 

###### normally, the grid setting is 2.54oo mm.

![image](../img/week8/ele_d40.png) 

###### select layers on the right side
F. Cu: Front_Cupper

B. Cu: Back_Cupper

Edge.Cuts: draw and outline the PCB board

![image](../img/week8/ele_d41.png) 

##### 2. Place

![image](../img/week8/ele_d44.png) 


##### 3. Outline the PCB board

Do this on Edge.Cuts layer.

![image](../img/week8/ele_d42.png) 

![image](../img/week8/ele_d43.png) 

##### 4. Route
Draw the routes.
Be careful, leave more space between the routes, and the angles should not be right angle.

![image](../img/week8/ele_d45.png) 

##### 5. 3D viwer
![image](../img/week8/3dviewer.png) 