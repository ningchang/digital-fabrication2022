+++
image = "portfolio/img/week12/mb_103.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 12: Machine Building"
draft = false
weight = 2
+++

---
<!--more-->
> Machine Building

###### Group page 
https://aaltofablab.gitlab.io/machine-building-2023-group-a

##### Team Friday members:
Hiski Huovila /

YaNing Chang / 

Zhicheng Wang / 

Dikai Yu /


##### Outline
- Principles (as an assembler)
- What I learnt (for the future machine design)
- Stage 1. Open box
- Stage 2. YZ - axis assembly
- Stage 3. X - axis & Extruder assembly


> #### Principles (as an assembler):
##### 1. Clean the machine assembly space

##### 2. Manage the storage and utilization of components: 
Follow the steps to get the components we need because the components  are small and similiar, and it is best to store them in a particular place or a box.(DO NOT spread components here and there, it will take you a lot of time to find the components).

##### 3. Use label for reference :
There is a sheet of paper with all the components printed on it. It helps to make sure you are using the correct components.

##### 4. Check the following steps and the necessary to tighten the screws fully:
The following steps probably need to install another component which could influence how tight you need to screw at this step, so it would be better to check the manual first.

> #### What I learnt from this process (for the future machine design):
##### The machine contains different parts
###### 1. Start with a concept 

###### 2. Controller -
Microcontroller boards
- Input deveice
- Output deveice : connect to machine parts


###### 3. Hardware -
1. Moving parts - rotation, spinning, conveying...etc.
      - motors
      - a belt and its holder

2. Holders and design space for holders - to hold a certain components.

3. Joints and Connections (need more specific dimensions)-
      - How to connect the different parts. Mainly metal parts (screws, nuts, pulleys)
      - Does the component rotate or rotate or stay fixed?
      - If so, what metal components connect the bracket to this feature?

4. Outer shell - to protect or cover the internal structure or contents.


> ##### Stage 1. Open box
At the beginning, we got this box and started the journey of building a 3D printer!
![image](../img/week11/mb_001.png)

The box contains several packages with components and a bag of HARIBO. :D
![image](../img/week11/mb_002.png)

It also includes four boxes. Make sure all the components for machine building are here.
![image](../img/week12/mb_003.png)

> ##### Stage 2. YZ - axis assembly
![image](../img/week12/mb_004.png)


![image](../img/week12/mb_005.png)


![image](../img/week12/mb_006.png)


![image](../img/week12/mb_007.png)


![image](../img/week12/mb_008.png)


![image](../img/week12/mb_009.png)


![image](../img/week12/mb_010.png)


![image](../img/week12/mb_011.png)


![image](../img/week12/mb_012.png)


![image](../img/week12/mb_013.png)


![image](../img/week12/mb_014.png)


![image](../img/week12/mb_015.png)


![image](../img/week12/mb_016.png)


![image](../img/week12/mb_017.png)


![image](../img/week12/mb_018.png)


![image](../img/week12/mb_019.png)


![image](../img/week12/mb_020.png)


![image](../img/week12/mb_021.png)


![image](../img/week12/mb_022.png)


![image](../img/week12/mb_023.png)


![image](../img/week12/mb_024.png)


![image](../img/week12/mb_025.png)


![image](../img/week12/mb_026.png)


![image](../img/week12/mb_027.png)


![image](../img/week12/mb_028.png)


![image](../img/week12/mb_029.png)


![image](../img/week12/mb_030.png)


![image](../img/week12/mb_031.png)


![image](../img/week12/mb_032.png)


![image](../img/week12/mb_033.png)


![image](../img/week12/mb_034.png)


![image](../img/week12/mb_035.png)


![image](../img/week12/mb_036.png)


![image](../img/week12/mb_037.png)


![image](../img/week12/mb_038.png)


![image](../img/week12/mb_039.png)


![image](../img/week12/mb_040.png)


![image](../img/week12/mb_041.png)


![image](../img/week12/mb_042.png)


![image](../img/week12/mb_043.png)


![image](../img/week12/mb_044.png)


![image](../img/week12/mb_045.png)


![image](../img/week12/mb_046.png)


![image](../img/week12/mb_047.png)


![image](../img/week12/mb_048.png)


![image](../img/week12/mb_049.png)


![image](../img/week12/mb_050.png)


![image](../img/week12/mb_051.png)


![image](../img/week12/mb_052.png)


![image](../img/week12/mb_053.png)


![image](../img/week12/mb_054.png)


![image](../img/week12/mb_055.png)


![image](../img/week12/mb_056.png)


![image](../img/week12/mb_057.png)


![image](../img/week12/mb_058.png)


![image](../img/week12/mb_059.png)


![image](../img/week12/mb_060.png)


![image](../img/week12/mb_061.png)


![image](../img/week12/mb_062.png)


![image](../img/week12/mb_063.png)


![image](../img/week12/mb_064.png)


![image](../img/week12/mb_065.png)


![image](../img/week11/mb_066.png)


![image](../img/week11/mb_067.png)


![image](../img/week12/mb_068.png)
YZ-axis is finished!


> ##### Stage 3. X-axis & Extruder assembly
![image](../img/week12/mb_069.png)


![image](../img/week12/mb_070.png)


![image](../img/week12/mb_071.png)


![image](../img/week12/mb_072.png)


![image](../img/week12/mb_073.png)


![image](../img/week12/mb_074.png)


![image](../img/week12/mb_075.png)


![image](../img/week12/mb_076.png)


![image](../img/week12/mb_077.png)


![image](../img/week12/mb_078.png)


![image](../img/week12/mb_079.png)


![image](../img/week12/mb_080.png)


![image](../img/week12/mb_081.png)


![image](../img/week12/mb_082.png)


![image](../img/week12/mb_083.png)


![image](../img/week12/mb_084.png)


![image](../img/week12/mb_085.png)


![image](../img/week12/mb_086.png)


![image](../img/week12/mb_087.png)


![image](../img/week12/mb_088.png)


![image](../img/week12/mb_089.png)


![image](../img/week12/mb_090.png)


![image](../img/week12/mb_091.png)


![image](../img/week12/mb_092.png)


![image](../img/week12/mb_093.png)


![image](../img/week12/mb_094.png)


![image](../img/week12/mb_095.png)


![image](../img/week12/mb_096.png)


![image](../img/week12/mb_097.png)


![image](../img/week12/mb_098.png)


![image](../img/week12/mb_099.png)


![image](../img/week12/mb_100.png)


![image](../img/week12/mb_101.png)


![image](../img/week12/mb_102.png)


![image](../img/week12/mb_103.png)

X-axis is finished!
