+++
image = "portfolio/img/week14/mc_080.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 14: Molding and Casting"
draft = false
weight = 2
+++

---
<!--more-->
> Molding and Casting

I would like to cast a plate with really thin edges in this session.
Since I was trying to cast a delicate cement-based component, the edge was broken because I used a mould made of hard material, which is difficult to remove the casting from the mould.

Therefore, I plan to make a small plate with cement or other material,and I set the thickness of edge is 3 mm this time. 

> ##### Outlines
This session contains three parts:
###### A. 3D mold building
1. Object
2. Negative mold
3. Positive mold

###### B. 3D Milling with Roland MDX-40 
1. facing
2. 3D roughing
3. 3D Finishing
    
###### C. Casting
1. Sillicone mold
2. Casting the object
    


###### Previous work 
![image](../img/week14/mc_05.png)



> #### A. 3D mold building

- Note:

Find a suitable margin for milling:
The margin between the object and mold influences the milling time. This time 15 mm for a margin is too much; it would be better to set a 10 mm margin.


###### 1. Setting the parameters
![image](../img/week14/mc_01.png)

###### 2. Body - the tiny plate
![image](../img/week14/mc_02.png)
![image](../img/week14/mc_06.png)

###### 3. Mould for Milling (Left)
![image](../img/week14/mc_04.png)

###### 4. Mould for casting
![image](../img/week14/mc_03.png)


> #### B. 3D Milling with Roland MDX-40 
###### Software: Vcarv Pro - toolpath 
###### Process:
1. Test with the foam
- Facing
- 3D roughing
- 3D Finishing

2. Wax Mold
- Facing
- 3D roughing
- 3D Finishing

###### Tools:
1. Butterfly end (22 mm, the shank is 6mm): Facing
2. Flat End (6 mm; two flutes): 3D Roughing
3. Ball nose end (3 mm): 3D Finishing

###### Notes:
1. Foam test: 
-   The chip load takes "MDF" as the reference
-   Stepover: 40%
-   Stepdown: 2 mm (the foam is softer than wax so the setting is like this)
-   (VPanel) Cutting speed: starts with 50%, if the millling goes well, then gradually increase the speed.

2. Wax mold: 
-   The chip load takes "Steel" as the reference
-   Stepover: 40%
-   Stepdown: 1 mm (3D roughing); 0,5 mm (Finishing)
-   (VPanel) Cutting speed: starts with 10 %, if the millling goes well, then gradually increase the speed.

3. Vcarv Pro setting
- Thickness: After facing, the material thickness will be thinner. So, the thickness in 3D Roughing and Finishing are less than the thickness in Facing. 


###### Facing (Foam)

Jobsetup (Foam)

![image](../img/week14/mc_002.png)

Draw a rectangle to cover the whole surface
![image](../img/week14/mc_003.png)

Make a Pocket toolpath
![image](../img/week14/mc_004.png)

In this stage, use butterfly end (22mm)
![image](../img/week14/mc_005.png)

- Cut Depth : the thickest thickness for the milling. It will influence the material thickness for the following steps (Roughing and Finishing).
This time, the cut depth is 10 mm for the facing.

- Pocket Allowance = -11
to prevent the corner like this (see below)

![image](../img/week14/mc_007.png)

![image](../img/week14/mc_006.png)



![image](../img/week14/mc_008.png)

![image](../img/week14/mc_009.png)

> Roland MDX-40 and VPanel

Set G54 as the connected "machine" and "Origin Point" 
![image](../img/week14/mc_010.png)

Set Z origin
![image](../img/week14/mc_011.png)

Set XY origin
![image](../img/week14/mc_012.png)

![image](../img/week14/mc_013.png)

Starts with lower cutting speed 
![image](../img/week14/mc_014.png)

Delete previous files and Upload the File for this milling
![image](../img/week14/mc_015.png)

Increas the cutting speed if the milling goes well
![image](../img/week14/mc_016.png)


###### Roughing and Finishing (Foam)

Jobsetup (Foam)
- Remeber to adjust the thickness fter facing! 
(The thickness is 10 mm less than the thickness (50,3 mm) before facing.)
![image](../img/week14/mc_017.png)

Import the 3D model
1. Initial Orientation: Top
2. Units: mm
3. Remeber to center the model
![image](../img/week14/mc_018.png)

4. Zero Plane Position in Model
Adjust the Zero plane below the model a bit
![image](../img/week14/mc_019.png)

![image](../img/week14/mc_020.png)

![image](../img/week14/mc_021.png)

![image](../img/week14/mc_022.png)

###### Roughing - Material Setup

Select 3D Roughing

![image](../img/week14/mc_023.png)

###### Tool: Flat End mill 6mm

- Pass Depth: 2mm (foam); 1 mm (wax roughing)

![image](../img/week14/mc_024.png)

- Machining Limit Boundary: Model Boundary

- Machine allowance: 0,1 mm

![image](../img/week14/mc_025.png)


###### Finishing - Material Setup
Select 3D Finishing

![image](../img/week14/mc_026.png)


###### Tool: Ball nose end 3 mm
![image](../img/week14/mc_027.png)

![image](../img/week14/mc_028.png)

###### Save the files separately
![image](../img/week14/mc_029.png)

![image](../img/week14/mc_031.png)

![image](../img/week14/mc_082.png)

###### Roughing and Finishing (Wax)
![image](../img/week14/mc_032.png)

![image](../img/week14/mc_033.png)

![image](../img/week14/mc_034.png)

![image](../img/week14/mc_035.png)

![image](../img/week14/mc_036.png)

![image](../img/week14/mc_037.png)

![image](../img/week14/mc_038.png)

![image](../img/week14/mc_039.png)

![image](../img/week14/mc_040.png)

![image](../img/week14/mc_041.png)

![image](../img/week14/mc_042.png)

![image](../img/week14/mc_043.png)

![image](../img/week14/mc_044.png)

![image](../img/week14/mc_045.png)

![image](../img/week14/mc_046.png)

![image](../img/week14/mc_047.png)

![image](../img/week14/mc_048.png)

![image](../img/week14/mc_049.png)

![image](../img/week14/mc_050.png)

![image](../img/week14/mc_051.png)

![image](../img/week14/mc_052.png)

![image](../img/week14/mc_081.png)

> ##### Silicone Negative Mold 
![image](../img/week14/mc_053.png)

![image](../img/week14/mc_054.png)

![image](../img/week14/mc_055.png)

![image](../img/week14/mc_056.png)

![image](../img/week14/mc_057.png)

![image](../img/week14/mc_058.png)

![image](../img/week14/mc_059.png)

![image](../img/week14/mc_060.png)

![image](../img/week14/mc_061.png)

![image](../img/week14/mc_062.png)

![image](../img/week14/mc_063.png)

![image](../img/week14/mc_064.png)

![image](../img/week14/mc_065.png)

![image](../img/week14/mc_066.png)

![image](../img/week14/mc_067.png)

![image](../img/week14/mc_068.png)

![image](../img/week14/mc_069.png)

![image](../img/week14/mc_070.png)

![image](../img/week14/mc_071.png)

![image](../img/week14/mc_072.png)

![image](../img/week14/mc_073.png)

![image](../img/week14/mc_074.png)

![image](../img/week14/mc_075.png)

![image](../img/week14/mc_076.png)

![image](../img/week14/mc_077.png)

![image](../img/week14/mc_078.png)

> ##### Object
![image](../img/week14/mc_079.png)

![image](../img/week14/mc_080.png)
