+++
image = "portfolio/img/week17/wc_07.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 17: Wildcard - Waterjet cutting"
draft = false
weight = 2
+++

---
<!--more-->
> Wildcard - waterjet cutting

![image](../img/week17/wc_07.png)

In this session, I cut with concrete and brass, and the texture and thickness of these two materials are quite different. And the following contents are the notes from when I cut with the waterjet machine.

> ##### Preparing files
###### Software: Rhino, Set the parameters for adjustable dimensions.

I prepared 2D files with Fusion, and the file's dimensions changed in Rhino. So, I redrew the files again.

1. The edge distance between the two bodies is preferably greater than 5mm, but depends on the material.

![image](../img/week17/wc_01.png)

2. The cutting path is better to be continuous.

I drew a component in which the distance between two cuts is smaller than 1mm (as in the picture); Kari (the master of the waterjet workshop) told me that it is still risky even though the work material is metal.

![image](../img/week17/wc_05.png)


So, I modified the original component ( a piece of metal with two tiny holes) into a piece of metal with two openings.

![image](../img/week17/wc_02.png)

![image](../img/week17/wc_03.png)


> ##### Dimensions of work materials

1. The size of the whole cutting material should be big enough.
2. Leave some space for clamps. (see the photo below)
3. Leave extra space for the injecting hole. (see the photo below)
4. The maximum height of the work material is around 40 mm.

![image](../img/week17/wc_06.png)

![image](../img/week17/wc_09.png)

##### Prepare work material
1. To prevent the nozzle of waterjet broken, the surface of the work material is better to be flat. 

2. Both sides of the work material are better parallel each side. (But not necessarily, just be aware that the maximum height of the work material is within 40 mm).


{{<video src="../img/week17/wc_11.mp4" controls="yes" width="640" height="385">}}

> ##### Accuracy:
The cutting accuracy would change depending on the parameters and work materials.

> ##### Result
![image](../img/week17/wc_08.png)

Soldering the components into a structure.
![image](../img/week17/wc_10.png)