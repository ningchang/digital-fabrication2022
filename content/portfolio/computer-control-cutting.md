+++
image = "portfolio/img/bottom_corner.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 05: Computer controlled cutting"
draft = false
weight = 2
+++

---
Vinyl Cutter and Laser Cutter

<!--more-->
![image](../img/00vinylcutter.png)  
![image](../img/01viny_roll.png)  
![image](../img/02roll.png)    

**Step1. Turn on the Vinyl Cutter, press the MENU to choose the Mode:**
There are 3 Modes:
1. ROll  
2. Piece: for small piece 
3. EDGE

**STEP2. Measure the size of the Vinyl paper**
![image](../img/05measure.png) 

**STEP3. Set the speed and force**
![image](../img/06speedforce.png)  

**STEP4. Set the origin**
![image](../img/07origin.png)  

**STEP5. Before printing**
![image](../img/09prepare.png)  
![image](../img/10prepare02.png)   

** Remember to tik "move to the origin"
![image](../img/11movetoorigin.png)  

** If use the heat transfer, remember to mirror the documents.**
![image](../img/17mirror.png)  

**STEP6. Heat Press**
![image](../img/heatpress01.png) 

Measure the temperature of  the heat press.
![image](../img/heatpress02.png) 

> **Setting:**
130 Celsius degree  
10 seconds
![image](../img/heatpress03.png) 

> **Vinyl Cutter**
Speed: 1 cm/s  
Force: 100 gf  
Thickness: 0.250 mm  


![image](../img/19ai.png) 

![image](../img/20cut.png)  

![image](../img/21bag.png)  
  
**Problems for Vinyl Cutting**

This time I would like to use a heat transfer stick.
However, it is difficult to peel off the target shapes printed on the heat transfer vinyl because there are many tiny details. Take the leaf stem as an example. It is too thin to peel off.
Thus, it is better to design simple shapes or have more patience.  

> **Laser cutter**


**Problems for Laser Cutting**

**1. Line with multiple dots and the Smooth line**  

![image](../img/vinyl_01draft.png)   
![image](../img/laser_failed.png)  
 
I would like to engrave some complicated pictures with the laser cutter for the first time. However, it took much more time than I expected.
Then I got the finished product, and there are only dots on the wood board instead of a complete line.

> Making the lines smoother and reducing the speed could fix the problem.

**2. Kerf**  
Process type : Vector  
Auto focus : Plunger   
Speed : 12%  
Power : 100%  
Frequency : 10%  

This time I would like to make a non-logical scene as this sketch.

![image](../img/00sketch.png)  


![image](../img/01draft.png)

![image](../img/bottom_corner.png)  
![image](../img/corner01.png)  
![image](../img/corner02.png)  
![image](../img/corner03.png)  
![image](../img/bottom.png)  
The grooves are a bit bigger for the insert part, making the whole components unsteady. Therefore, it is necessary to consider the stroke thickness to fix the unstable structure.  

For example,  
According to the original parameter, the groove is loose, and the insert part is smaller.  

**If I want to adjust the groove**,  
I need to minus 0.01 mm two times for the height of the square track because there are two borders that I would cut.  

**When it comes to the insert part**,  
if I would cut only one border, I need to plus 0.01 mm for the height or width.   
