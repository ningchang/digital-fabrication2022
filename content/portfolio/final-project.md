+++
date = "2016-11-05T19:41:01+05:30"
title = "Final Project"
draft = false
image = "portfolio/img/finalproject/fp_05.png"
showonlyimage = false
weight = 1
+++


<!--more-->
**Current status**

- Magnetic Field Strength Measurement / Hall effect sensor

Check the minimum volume of a circuit board with main components for this project.

![image6](../img/finalproject/fp_06.png)

![image6](../img/finalproject/fp_09.png)

![image6](../img/finalproject/fp_08.png)

![image6](../img/finalproject/fp_07.png)

![image6](../img/finalproject/fp_10.png)

###### References
1. https://circuitdigest.com/microcontroller-projects/arduino-magnetic-field-measurement

2. https://www.allegromicro.com/en/insights-and-innovations/technical-documents/hall-effect-sensor-ic-publications/hall-effect-ic-applications-guide

3. https://www.electronicshub.org/hall-effect-sensor-with-arduino/




**Escaping Fireflies**

###### Idea 
The idea is to imitate the fireflies, which will fly away once the human is close to them.

One of my inspirations is the "magical portraits" in "Harry Potter". The person would be alive and escape from the original portrait frame to another one. And this idea was inspired by these escaping portraits.

I plan to use a dark and translucent acrylic board to cover the LEDs. And LEDs will dim once the audience is close to this installation piece; meanwhile, the amounts of LEDS increase in another piece. (Try to imitate the fireflies' escape from the audience.)

![image6](../img/finalproject/fp_03.png)

![image6](../img/finalproject/fp_04.png)





**Idea in 2022:Under the blanket**

Do you remember a chapter of The Little Prince talking about the animals under a hat? Have you seen your pet sleep under your blanket? Did your little brother hide under a quilt? 

![image1](../img/little_prince.jpeg)

> This project aims to make a series of installation that looks like animals or something under the blanket. To imitate a living creature, I would like to design an electronic device to make this stuff breathe. Yet, I plan to make its appearance like a pet under the blanket, which looks soft and makes people want to pat it and interact with it.

![image2](../img/sketches39.png)

![image3](../img/sketches40.png)

![image4](../img/sketches41.png)

>To imitate a living creature, I would like to focus on how to make this object breathe like a real one.

Then, it is crucial to inflate and deflate gently and gradually and naturally change its shape or size.  


For this reason, there are some points I need to consider how to achieve them.
1. While deflating,   
- the surface of the object will not become wrinkled or folded.  
- the object won’t collapse or be flat.  

Then, since its structure can support the surface. I would like to imitate the form of a sponge, which can be squeezed and recovered soon.  

For this part, the form could be printed through 3d printing. 

![inspiration](../img/sponge_skeleton.jpeg)

![image5](../img/sketches42.png)

![image6](../img/sketches43.png)   


> 2. When it comes to inflate and deflate, except for the air pump device. 
I have a thought:  
Is it possible to make a device that works as lungs and diaphragm that can be installed into this object?   
  
Thus, I would like to do experiments to test the air pump and the following device.  

![lungs and diaphragm](../img/lungs.jpeg)  
  
{{ < youtube 6oMFAMqSlq4 > }}  

I sketch the structure like this in the picture below to imitate the breathing system.   

And I would like to make a simple electronic device to pull and let go of the membrane(the pink part in the picture) to imitate the animals' breathing.   

![image6](../img/sketches47.png)

