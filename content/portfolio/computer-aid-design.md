+++
image = "portfolio/img/freecad_1.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 04: Computer-aided Design"
draft = false
weight = 2
+++


<!--more-->

> Since I would like to make organic form for my final project, I tried to use Blender to present the form.

**Tools: Blender**

![image](../img/form0.png)  

![image](../img/form.png)  

![image](../img/form2.png)   

> Design a device to mimicry lung for the final project with Free CAD.    
**Tools: Free CAD**

##### Step1: Download and install Free CAD.
![image](../img/freecad_1.png)  

##### Step2: make a new file to start
Set default benches to “Part Design.”  

![image](../img/freecad_2.png)

And see the Combo View > Model View.  

![image](../img/freecad_3.png)  


##### Step3. To Specify a few variables.

Thus, we should Switch to the “Spreadsheet.”
And make a new spreadsheet.
- Device Height
- Device Diameter
- Material thickness

Then go to the Properties > Alias and Set the variable name.


And save it.

##### Step4: Back to Part Design and create a body 

##### Step5. Create a body



And set it to active
Right flick and click Toggle active body.
If there are different bodies to design, enable to work with a specific body.
Step 6: Create a Sketch

! The orientation is essential, keep we can look at the object from the FRONT side.

To choose a plane, we are looking at



And the workbenches will automatically switch to Sketcher. Then we can start drawing.  