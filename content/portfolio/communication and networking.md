+++
image = "portfolio/img/week15/nw_49.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 15: Networking and Communications"
draft = false
weight = 2
+++

---
<!--more-->

###### A. Communication between one board and the device
###### B. Serial communication between two boards

>In this communication session, it can be diveided into five parts.
- Part 1 Create an access point
- Part 2 Server and client
- Part 3 FirstLine
- Part 4 Extract Query
- Part 5 create responses


Microcontrollerboard: XIAO ESP32C3

Output device: NeoPixel


> ##### Part 1 Create an access point

- I set ESP32 as the server. It works yet no IP.
![image](../img/week15/nw_001.png)

> ##### Part 2 
- In this session, we will do something to the server and client sides.

- Then, there was an error because I mistyped the language. It should be WiFIServer server, instead of "serve".

![image](../img/week15/nw_005.png)

- Initially, I connect the ESP32C3, it works. However, when I was going to do the server and client class, which need IP address. So, I change the WiFi server as the WiFi in my liveing place.
![image](../img/week15/nw_007.png)

- To test if the client side works, check with "curl".
* Terminal > give a command "curl IP address"

![image](../img/week15/nw_008.png)

- (Termical/client) The right side shows the response.
- (Server) The left side shows if client connects.
![image](../img/week15/nw_009.png)

![image](../img/week15/nw_010.png)

![image](../img/week15/nw_011.png)

![image](../img/week15/nw_012.png)

![image](../img/week15/nw_013.png)

![image](../img/week15/nw_014.png)


> ##### Part 3 FirstLine

![image](../img/week15/nw_015.png)

- However, when I sent “turn LedOn”, it didn’t show on the monitor. So, I tried to reconnect against works. 
![image](../img/week15/nw_016.png)

![image](../img/week15/nw_017.png)

![image](../img/week15/nw_018.png)

> ##### Part 4 Extract Query

- Board library > Regexp (Install)
![image](../img/week15/nw_019.png)

![image](../img/week15/nw_021.png)

- Shows an Error
![image](../img/week15/nw_022.png)

- Change “incoming.charAt” to “firstLine.”
![image](../img/week15/nw_023.png)

- The modified version
![image](../img/week15/nw_024.png)

![image](../img/week15/nw_025.png)

![image](../img/week15/nw_026.png)

![image](../img/week15/nw_027.png)

![image](../img/week15/nw_028.png)

![image](../img/week15/nw_029.png)

![image](../img/week15/nw_030.png)

![image](../img/week15/nw_031.png)

![image](../img/week15/nw_032.png)

![image](../img/week15/nw_033.png)

When I sent other message, it didn’t show the ”Unknown request”

*Then, I found that I did not leave the space between else and {
![image](../img/week15/nw_034.png)

![image](../img/week15/nw_035.png)

> ##### Part 5 create responses
![image](../img/week15/nw_036.png)

![image](../img/week15/nw_037.png)

![image](../img/week15/nw_038.png)

![image](../img/week15/nw_039.png)

![image](../img/week15/nw_040.png)

![image](../img/week15/nw_041.png)

![image](../img/week15/nw_042.png)

![image](../img/week15/nw_043.png)

{{<video src="../img/week15/nw_044.mp4" controls="yes" width="640" height="385">}}

![image](../img/week15/nw_48.png)

![image](../img/week15/nw_49.png)

#### Serial communication between two boards
Rp2040 and ESP32C3

In this part, RP2040 is being the sender and ESP32C3 is the reciever.

- ESP32C3 
![image](../img/week15/nw_045.png)
This could not work, and I asked chat GPT how to fix the problem.
And it suggest to make the value bigger. (originally, the value is 5, and it is not enough. So I change the value into 6, and it works.)
![image](../img/week15/nw_046.png)

- The sender Board kept sending Hello to the reciever, but the reciever did not respond......
![image](../img/week15/nw_047.png)

Haven't fixed the problem yet.
