+++
image = "portfolio/img/3s_laptop05_smoothfusion02.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 07: 3D scanning and printing"
draft = false
weight = 2
+++

---
<!--more-->
> 3D SCANNING 



![image](../img/3s_leo_box.png)    
In the box:  
- 3D Scanner (Leo)  
- TWO cables:   
    1. power cable with power supply   
    2. net cable: transfer or download the data    

![image](../img/3s_leo_box01.png)  
![image](../img/3s_leo01.png)  

Step 1. Turn on Leo:
1. Press and hold it

![image](../img/3s_leo_power.png)  
![image](../img/3s_leo_power01.png)  


Step 2. Touch screen  
See how much spaces left for scanning: On the upper part of the touch screen. 
 - 1 scan takes 500 Mb

Step 3. Start a new project    
Take Multiple takes of the object , and use computer software to stitch them together.  
1. Hit “NEW PROJECT”  
![image](../img/3s_new_project.png)   
![image](../img/3s_new_01.png)   
![image](../img/3s_new_02.png)

Step 4. Start recording (2 ways)
1. Option 1: hit it to record,press the red circle on the buttom of the screen
2. Option 2: press the button over here.
![image](../img/3s_leo_start.png)  
To stop the scan: press the START or the button  

Step 5. Scan the object  
- Environment: make sure the environment as clean as possible

- Start:while moving around the object, the surfaces become green. 
To hold the capture and capture it become green.

Red areas means somewhere aren’t captured.     
- Add another take
    1. Go back to”Project”
    2. It enters into the project directory
    3. Hit ”Add Scan” to add another scan
> It is important to point the ground, detect the best plane, then start the scanning.

At first, we tried to scan some small metal-made object, but it was difficult for the 3D scanner to capture.  
Then, we took this cup as the object. Yet, it is not easy to scan the inner suface of the object because the depth of the cup is too deep to scan.  
Thus, we decided to scan each other to complete this assignment.    
  
  
![image](../img/3s_metal.png) 
![image](../img/3s_cup02.png) 
![image](../img/3s_scan_finish.png) 
  

Step 6. Name the scanning
- to hit the edit item. 
  

Step 7.Import to the computer    
1. Open ArtecStudio 15: to import the scanning  
2. Hit “import from Leo”  
3. Show No scanner found  
 4. Connect by IP  
        Back to "Leo"
        - Hit “project”  
        - Go to the main screen  
        -  Hit ”Setting” > Network  
        ( Make sure to use the same network as the computer too)
        - Hit the little arrow   
        It is able to see the IP address, and enter it into the computer screen.  

5. Back to the computer  
        -  hit”Connect” (Make sure scanner is on, because you are uploading the project)
6. Import the scan
7. There are different groups, each group can contain several takes. You can filter some out.  


Step 7. Orthgonal view  
It’s difficult to view from all of the angles at a time. In order to select all of the object you want to remove in an efficient way.
- View > Orthgonal view  


Step 8. Clean  
    1. Do it one by one.  
    2. Hit “editor> Eraser> Lasso selection  
    3. Holding down “Ctrl + Clicking and dragging “ to select  
  
![image](../img/3s_laptop02_editor.png) 
![image](../img/3s_laptop03_remove_spot.png)  

Step 9. Align  
Align> auto-alignment  

![image](../img/3s_laptop01_align.png) 

Step 10. Smooth fusion and fast fusion 
- to fuse all scans together in one 

![image](../img/3s_laptop04_smoothfusion01.png) 
![image](../img/3s_laptop05_smoothfusion02.png) 
![image](../img/3s_laptop06_fastfusion01.png) 
![image](../img/3s_laptop07_fastfusion02.png) 

Step 11. Focus on fast fusion  
![image](../img/3s_laptop08_focus_fastfusion.png) 

Step 12. Modify (remove spots and hole filling)

1.  To remove some small objects
    -  “small object filter”
2. Hole filling
    -   Fix holes
    -   Show individual hole here > select all
    -   Fill holes
3. But it seems there are some wired thing
     Tools> Mesh simplification > apply

Step 13. Texture  
1. Texture> select texture source> Apply  
2. Export  
3. Enable texture normalization    
![image](../img/3s_laptop09_texture.png)     

Step 14. Export  
File > Export > Meshes  

Choose “obj” format.This format also saves the texture.   
- "Obj" is also accepted by 3D printing.    
- “STL.” : If you want to save something directly in 3D print.    
![image](../img/3s_laptop10_export.png) 
![image](../img/3s_laptop11_export_obj.png) 

Step 15. Save the texture  
Save as png.(Lose less quality).   
![image](../img/3s_laptop12_format_png.png) 

Step 16. Switch to another software

File> import > obj
- “mtl.”:shows material
- “Obj.”: contains the mashes( choose this one)  

>  3D PRINTING  

Ultimaker: can leave for overnight.  
LuzBot : should use the data during the opening time and make sure the user is here until printting finished.

Step 1: Load the filament
- The filament is stored in rolls  
- Here we use the wider filament 2.85 "PLA"  

Ultimaker 3 +
1. Load the material
2. Change
3. Heat the nozzole
4. Clip the filament with a clipper
5. Select pLA, and the nozzle size: 0.4mm

Step 2. Software
- "Cura" for Ultimaker 3+  
- Open the file  
File> open file  

- At the begging , it is better to print just one object because it could be faster.  
-   If the settings are wrong, you can reset it earlier.  
" Start the small thing, try to understand the machine."  
![image](../img/3d01_place.png) 

Step 3. Print setting

- Layer Height: 0.15
 Choose 0.06 can print it more delicate but it could be jammed. Thus, it is better to try 0.15 or 0.2.  

- Infill
    Define how much infill your object will have.
    If you have no infill: the object only has outlines

-   20% is the default
-   100% is not recommended.
    If the object is bigger, it would have to some problems.Once the layers been laid down, the filament of the base is hot. Though maybe the bottom (first) layer is not shrinked. However,ther layers are heated. So, when the all the layers down, they would shrink, and those layers could shrink more. And the corner could flip up.

- The more infill means more material shrinked, the more material here the more material upward.

- It is better to set the infiller between in 10 and 20 % as for starting.
- Then, to find the balance between what you want to get and the machine can offer.

![image](../img/3d01_print_setting.png) 

Step 4. Slice
![image](../img/3d03_slice.png) 

Step 5. Save as gcode and transfer with SD card
![image](../img/3d04_save_gcode.png) 
![image](../img/3d05_save_name.png) 

Step 6.Put the SD card to the printer
![image](../img/3d06_sdcard.png) 

Step 7. Make sure the print base is clean, clean with isopropernol
![image](../img/3d07_print.png) 
![image](../img/3d08_select.png) 
![image](../img/3d09_heating.png) 

Step 8. The problem I met  
When I opened the software, I forgot to choose Ultimaker 3+ as my printer.
Then, the screen of the printer showed like this. So, I went back to the menu, selected the machine, and solved the problem.   
> ![image](../img/3d10_problem.png) 
![image](../img/3d11_remeber.png) 
![image](../img/3d12_reselect.png) 
![image](../img/3d13_heating.png) 
![image](../img/3d14_show.png) 
![image](../img/3d15_print01.png) 

Step 9.
If you want to leave, it is better to stay until finished the first layer.  
![image](../img/3d16_first_layer.png) 
![image](../img/3d17_print.png) 
![image](../img/3d18_print_finish.png) 
![image](../img/3d19_final.png) 
