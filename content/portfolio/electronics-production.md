+++
image = "portfolio/img/pcb24_final.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 06: Electronics Production"
draft = false
weight = 2
+++

---
<!--more-->
#### PCB milling

> **Open the file in KiCad**
Check file

> **Open CopperCam**  

There is a board, then close it.  

> **File > Open > New Circuit**
Choose F_Cu
It is our main connection

F-Mask
Is for soldering mask, use the Vinyl cut

In this PCB Milling machine, we just need **F_Cu file**
Click "Yes"

> **File >Dimensions**

To make sure the margin around the design, 1 mm  
( here we changed the margin to 2 mm)  

**To make sure the white cross**  

The white cross should be at X,Y = (0,0) in the bottom left corner.   

**File > Origin**   
Set it ( 0,0 )    
![image](../img/pcb10_white_cross.png)  

To use PCB, need to use special tools
> **Parameter > Tool Library**
>
![image](../img/11pcb_tool_library.png)

> **To specify the tool will in stock**  
**Orange for Milling**  
    0.1-0.15 mm : to cut 0.15,0.2 mm traces  
    0.2-0.5 mm : to cut 0.4 mm channel; to engrave the first layer  

**Yellow label**  
    1 mm cutter: to cut out the contour board  

**Green for drilling**  

If you want to cut the double-side and you need some drills.  
It is only used for drill vertically, only for up and down  
-  0.8 mm- drill 1.4 mm holes 
-  2mm
-  3mm  

> **setting tools**
Back to Copper CAM.  
Tools are the numbers.  
 #2 for cutting the top layer 2-2.5 mm engraver  
    
**Specify the rotation:**
    Plunge speed: 1 m/s
    Parameter> select tools

**Engraving tools**
2> 2.5
    Engraving speed : 10 mm/s

**Cutting**
#3
    Cutting depth 2mm
    Our board thickness is 1.6 mm
    We need to make sure that it can cut the board through, so that set it as **2 mm**  

**Centering tool**
    Should did double-sided,but in this case, we don’t have to care this

**Drilling tools**
    If you would like to do double side board, then you can set 0.8 mm drilling. (This time, we use one-sided, so we do not need to care about this.)

![image](../img/pcb12_tool_setting.png)

> **Create contour**
1. Click calculate contour  
    It will calculate the geometry, then you can click the final rendering button to preview.  

![image](../img/pcb13_set_contour.png) 

> **To export tool path to the milling machine**  
1. click "Mill"
2. check some setting  
        Speed 
        XY zero point: Go with the “white cross”

3. To specify Z zero point: circuit surface
4. Save and create a new folder”Milling"
    name: updi-d11c, and save as ROLAND EGX.


![image](../img/pcb14_srm20.png)  
![image](../img/pcb15_one_sided.png)  

> **Use the Vpanel**

The panel can move around the spindle of the machine
Load the tools

> **Clean and make sure it stick well**

![image](../img/pcb17_srm20_clean.png)  
![image](../img/pcb18_srm20_flat.png) 

> **Z  zero**  

1. You should move the tools to as close as possible to the surface.
2. Mind the gap!
3. Release the set tool:the tool gonna move down to hit the surface of circuit board.
4. Close the set tool.  

![image](../img/pcb18_srm20_Z0.png) 

5. Confirm it is at Z zero: You can look at the panel on the left side of Panel interface and hit.     
6. Set Origin Point > hit “Z” Botton  
7. The left side of panel **Z : 0**    
![image](../img/pcb18_srm20_Z01.png) 

> **XY  zero**

1. Lift up the bit (Z axis)  
2. Then we can set X, Y zero.  
3. Confirm the origin point (X,Y), then the X,Y became 0,0  
![image](../img/pcb19_srm20_XY0.png)  

> **Engrave-T2 file**

1. Insert orange **0.2-0.5 mm milling bit**
2. Close the lid   
3. Clisk "Cut"  
4. Must to Delete all(  previous users’ files )  
![image](../img/pcb20_srm20_delete.png)    
5. Add (your files) : at first T2 file (depends on what type of your tools)  
6. Output
7. Resume

>**Cut out the board- T3 file**

1. Select yellow **1 mm cutter bit** for cutting
2. Insert the tool
3. Reset the Z zero
4. Cut > Delete previous file > Add **T3 file**

![image](../img/pcb21_srm20_t2.png)

![image](../img/pcb22_srm40_bit.png)   
![image](../img/pcb22_srm40_pcb.png)  

>**Test the traces**    
Turn on the device and press the yellow button to test the conduction of the PCB.    
![image](../img/test_conduction.png)  
![image](../img/pcb23_test.png)

> **Soldering**
The elements for soldering:    

![image](../img/so1.jpeg)    
![image](../img/so2.jpeg)  
![image](../img/so3.jpeg)   
![image](../img/so4.jpeg)   
![image](../img/so5.jpeg) 
![image](../img/so6.jpeg)       


> Problems we met and how to overcome   
**1.The copper traces were almost milled out**  

It could be a result of bit inserting. The bit is too low that it was engraved too deep, then the traces were milled out. However, we tried to reset 2-3 times, but it did not work.    

For this reason, we change to **MDX-40 milling machine**.  

> **2.This time, the copper traces were too light,so there are little traces on the board.**  

We checked every step carefully, reset Z zero and tried many times. Unfortunately, the bit still did not engrave clear traces. We felt frustrated.
Until we “changed to a flat place to put the Copper board” and made sure it stuck well, then, it worked!

> So, making sure the wood board is flat enough and sticking the copper board well is critical.

