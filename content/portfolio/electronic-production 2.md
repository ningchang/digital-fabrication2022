+++
image = "portfolio/img/week10/ep_10.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Week 10: Electronics production 2"
draft = false
weight = 2
+++


<!--more-->
###### 2022 Electronics production (One - sided board)
- This was making the ATtiny board

- The operations of the machine is same as the previous year, and you can check this webpage.

https://ningchang.gitlab.io/digital-fabrication2022/portfolio/electronics-production/

###### 2023 (double -sided board)
Last year, I only made a one-sided board, so this time I want to try double-sided one.


###### Note (double-sided board)

1. Double-sided board: It is easier to align the double-sided circuit with a symmetry outline.

2. The width of the tracks is better to be wider than 0,25 mm. If the width is too narrow, it isn't easy to solder


###### Soldering tips
1. Plug the pins into a breadboard before soldering. The angle between the component and the pins should be a right angle. 

2. Cut the soldering wires into small pieces and place them in the soldering area. LEDs are a tiny component, and placing the soldering wires in advance is better. 

###### About drawing a circuit board

During the KiCad process, make sure all the input devices and Output devices connected 

1. Electricity-wise: connect to GND, electricity supply.

2. Digital-wise: consider connecting "Data in" or "Data out". 

Some tiny devices (such as LEDs) only have two pins, and they integrate electricity and digital input and output.


###### Components
LED 1206
![image](../img/week10/ep_01.png)

Button
![image](../img/week10/ep_02.png)

![image](../img/week10/ep_03.png)


Circuit Board 
![image](../img/week10/ep_05.png)

- Bad soldering (as the photo below)
![image](../img/week10/ep_06.png)

The LEDs are too tiny to solder, and the soldering iron stayed in the soldering area for a long time, causing the increasing temperature of the board. 
The copper layer of the circuit board peeled off, which caused the disconnection.

Also, the tracks were too narrow, making erosion easier and causing disconnection.

At that time, I tried to fix the problem with copper tape. However, the track was too narrow to let the tape attach to. (yes, it should be wider than 0,25 mm or take lots of time to fix this problem.)


![image](../img/week10/ep_07.png)

Check the connection
![image](../img/week10/ep_08.png)

![image](../img/week10/ep_09.png)

Finally, I made another one.
![image](../img/week10/ep_10.png)

The other side (plug with XIAO board)
![image](../img/week10/ep_11.png)

Looks okay...
![image](../img/week10/ep_12.png)


But unfortunately, when I was doing the programming, I found that the button only connects to GND without connecting digital input and output. And I need to make a new one, if I want to do the programming.

![image](../img/week10/ep_13.png)
